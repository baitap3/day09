function search_data() {
    var depart = $("#select_depart option:selected").val();
    var name = $("#key_word_input").val();
    $.ajax({
        type: "POST",
        url: "get_sv.php",
        data: { "key_w": name, "depart": depart },
        success: function (response) {
            // xử lý phản hồi
            // document.getElementById("test-doc").innerHTML = JSON.stringify(JSON.parse(response));
            // let temp = JSON.parse(response);
            // document.getElementById("test-doc").innerHTML = temp;

            let result = JSON.parse(response)/* assume you have the data from PHP in a variable named 'result' */;
            var table_temp = document.getElementById('table_sv');
            var rowCount = table_temp.rows.length;
            for (var i = rowCount - 1; i >= 0; i--) {
                table_temp.deleteRow(i);
            }
            for (let i = 0; i < result.length; i++) {
                let data = result[i];
                let index = i + 1;

                let tableRow = document.createElement('tr');

                tableRow.innerHTML = `
            <td>${index}</td>
            <td>${data[1]}</td>
            <td style="width: 65%; text-align:left;">${data[2]}</td>
            <td class="action" style="width: 15%;">
                <button class="delete_change" onclick="document.getElementById('delete_${data[0]}').style.display='block'">Xóa</button>
                <button class="delete_change" onclick="edit_student('edit_${data[0]}').style.display='block'">Sửa</button>
            </td>
        `;


                table_temp.appendChild(tableRow);
            }
        }
    });
}

$("#select_depart").on("change", function () {
    search_data();
})

$("#key_word_input").keyup(function () {
    search_data();
})

function reset_table() {
    document.getElementById("key_word_input").value = "";
    $("#select_depart").val("nothing");
    search_data();
}

function del_row(id, id_modal) {
    // document.getElementById("test_function").innerHTML = id;

    var employeeId = id; //get the employee ID


    // Ajax config
    $.ajax({
        type: "POST", //we are using GET method to get data from server side
        url: 'delete_row.php', // get the route value
        data: { employee_id: employeeId }, //set data
        beforeSend: function () {//We add this before send to disable the button once we submit it so that we prevent the multiple click

        },
        success: function (response) {//once the request successfully process to the server side it will return result here
            // Reload lists of employees
            document.getElementById(id_modal).style.display = "none";
            search_data();

            
        }
    });



}

function edit_student(id){
    document.getElementById(id).submit();
}

// span.onclick = function () {
//     modal.style.display = "none";
// }


const collection = document.getElementsByClassName("modal");

// When the user clicks anywhere outside of the modal, close itF
window.onclick = function (event) {
    for (let i = 0; i < collection.length; i++) {
        // collection[i].style.display = "red";
        let modal = document.getElementById(collection[i].getAttribute("id"));

        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

}