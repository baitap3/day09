<?php
require_once "database.php";

function generateRandomString($length)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return "Nguyen Van " . $randomString;
}

$khoa = "";
$gender = "";
$n = 20;

$sql = "INSERT INTO SINHVIEN (HoVaTenSV, GioiTinh, MaKH, NgaySinh, DiaChi, HinhAnh)
        VALUES (?, ?, ?, ?, ?, ?);";


if ($stmt = $conn->prepare($sql)) {
    $i = 0;
    while ($i < $n) {
        if (rand(0, 10) > 5) {
            $khoa = "MAT";
            $gender = "1";
        } else {
            $khoa = "KDL";
            $gender = "0";
        }

        $stmt->bind_param(
            "ssssss",
            generateRandomString(3),
            $gender,
            $khoa,
            date("Y/m/d"),
            generateRandomString(10),
            generateRandomString(10)
        );    // "s" string type      
        $stmt->execute();
        $i++;
        // $stmt->bind_result($name, $salary, $ssn);
        // while ($stmt->fetch()) {
        //     printf("%s %s %s\n", $name, $salary, $ssn);
        // }
    }
}
$conn ->close();
echo "OK";
?>
