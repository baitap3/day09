<!-- https://viblo.asia/p/validate-du-lieu-form-don-gian-hieu-qua-hon-voi-jquery-validation-XL6lAkwJKek -->
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./update_students.css">
    <style>
        label::after {
            margin-left: 5px;
            font-size: 20px;
            content: "";
            /* color: red; */
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
</head>


<body id="body">

    <div class="container">
        <form style="border: 0;" method="post" action="update_sv.php" enctype="multipart/form-data">
            <?php
            include "database.php";
            $id = $_REQUEST["ID"];
            $sql = "Select * FROM SINHVIEN where ID like ?";
            $result = "";
            if ($stmt = $conn->prepare($sql)) {
                // echo $depart;
                $stmt->bind_param(
                    "s",
                    $id,

                );    // "s" string type      
                $stmt->execute();
                $result = $stmt->get_result()->fetch_assoc();
            }
            ?>

            <input type="text" name="ID" style="display: none;" value=<?= $id?>>

            <fieldset class="group_name" style="color : black">
                <label id="name" for="uname" style="color: white;">Họ và tên</label>
                <input type="text" name="unam" value=<?= "'" . $result["HoVaTenSV"] . "'" ?>>
            </fieldset>

            <fieldset class="group_gender">

                <?php
                $gender = array(0 => "Nam", 1 => "Nữ");
                $department = array(
                    "nothing" => "Chọn phân khoa", "MAT" => "Khoa học máy tính",
                    "KDL" => "Khoa học vật liệu"
                );
                echo $gender[$_POST["gen"]];
                ?>
                <label id="gender" for="gen">Giới tính</label>
                <?php

                for ($x = 0; $x < 2; $x++) {
                    if ($result["GioiTinh"] == $x) {
                        echo '<input class = "gender_option" checked="checked" type="radio" name="gen" value="' . $x . '"> ' . $gender[$x];
                    } else {
                        echo '<input class = "gender_option" type="radio" name="gen" value="' . $x . '"> ' . $gender[$x];
                    }
                }
                ?>
            </fieldset>

            <fieldset class="group_department">
                <label id="department" for="depart">Phân Khoa</label>
                <select id="select_depart" name="depart">
                    <?php
                    foreach ($department as $key => $value) :
                        if ($key == $result["MaKH"]) :
                            echo '<option selected="selected" value="' . $key . '">' . $value . '</option>';
                            continue;
                        else :
                            echo $key;
                            echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!
                        endif;
                    endforeach;
                    ?>
                </select>

            </fieldset>

            <fieldset class="date_group">
                <label id="date" for="d">Ngày sinh</label>

                <input id="date_input" data-date="" value=<?= $result["NgaySinh"] ?> data-date-format="DD/MM/YYYY" type="date" name="d" max=<?php echo $day
                                                                                                                                            ?>>
            </fieldset>

            <fieldset class="address_group">
                <label id="add_label" for="add">Địa chỉ</label>

                <!-- <input id="add_input" type="text" name="add"> -->

                <textarea id="add_input" rows="5" cols="42" name="add" wrap="hard"> <?= $result["DiaChi"] ?></textarea>
            </fieldset>

            <fieldset class="image_group">
                <label style="color: white;" id="img_label" for="fileToUpload">Hình ảnh</label>
                <!-- <img src=<?php //echo $result["HinhAnh"]; 
                                ?> onerror="this.onerror=null; this.src='default.jpg'" width="200px" height="150px"> -->


                <input type="file" name="fileToUpload" id="fileToUpload" accept=".jpg, .png, .jpeg"  >
                <!-- <img id="preview" class="preview-image" src="#" alt="Preview Image"> -->
            </fieldset>


            <button style="margin-left: 7cm;" id="my_button" name="button" type="submit" >Xác nhận</button>

         


        </form>

    </div>
    <script type="text/javascript" src="./update_students.js"></script>
</body>

</html>