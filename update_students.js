$("#date_input").on("change", function () {
    if (!$("#date_input").val()) {
        this.setAttribute(
            "data-date", "dd/mm/yyyy");
    } else {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
                .format(this.getAttribute("data-date-format"))
        );
    }

}).trigger("change");

// document.querySelector("#fileToUpload").onchange = function () {
//     const preview = document.querySelector("#preview");
//     const file = this.files[0];
//     const reader = new FileReader();
//     reader.onload = function (event) {
//         preview.src = event.target.result;
//     };
//     reader.readAsDataURL(file);
// }; 